webpackJsonp([68],{

/***/ 586:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaytmwalletloginPageModule", function() { return PaytmwalletloginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__ = __webpack_require__(676);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PaytmwalletloginPageModule = /** @class */ (function () {
    function PaytmwalletloginPageModule() {
    }
    PaytmwalletloginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__["a" /* PaytmwalletloginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__["a" /* PaytmwalletloginPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], PaytmwalletloginPageModule);
    return PaytmwalletloginPageModule;
}());

//# sourceMappingURL=paytmwalletlogin.module.js.map

/***/ }),

/***/ 676:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaytmwalletloginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PaytmwalletloginPage = /** @class */ (function () {
    function PaytmwalletloginPage(navCtrl, navParams, toastCtrl, apiCall, menu, toast, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.menu = menu;
        this.toast = toast;
        this.alertCtrl = alertCtrl;
        this.successresponse = false;
        this.inputform = false;
        this.razor_key = 'rzp_live_eZdWnBRmYRCLWo';
        this.paymentAmount = 150000;
        this.currency = 'INR';
        // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
        console.log("user details", this.islogin);
        this.paytmnumber = this.islogin.phn;
        this.parameters = navParams.get('param');
        console.log("parameters", this.parameters);
        if (localStorage.getItem('default_curreny') !== null) {
            this.currency = localStorage.getItem('default_curreny');
        }
        if (this.islogin.isSuperAdmin == true) {
            var alert_1 = this.alertCtrl.create({
                message: 'Do you want to Renew the vehicle?',
                buttons: [{
                        text: 'Yes',
                        handler: function () {
                            _this.updateExpDate();
                            (function (err) {
                                console.log(err);
                            });
                        }
                    },
                    {
                        text: 'No',
                        handler: function () {
                            // this.menuCtrl.close();
                        }
                    }]
            });
            alert_1.present();
        }
    }
    PaytmwalletloginPage.prototype.ionViewDidEnter = function () {
        this.menu.enable(true);
    };
    PaytmwalletloginPage.prototype.payWithRazor = function () {
        var orderId;
        // debugger
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/GO0jiDP.jpg',
            currency: this.currency,
            key: this.razor_key,
            amount: this.paymentAmount,
            name: this.parameters.Device_Name,
            // order_id: '123dknkdslnflk',
            prefill: {
                email: this.islogin.email,
                contact: this.paytmnumber,
                name: this.islogin.fn + ' ' + this.islogin.ln
            },
            theme: {
                color: '#d80622'
            },
            modal: {
                ondismiss: function () {
                    console.log('dismissed');
                }
            }
        };
        debugger;
        var that = this;
        var successCallback = function (success) {
            // alert('order_id: ' + JSON.stringify(success));
            that.showAlert(success.razorpay_payment_id);
            orderId = success.razorpay_order_id;
            var signature = success.razorpay_signature;
        };
        var cancelCallback = function (error) {
            console.log("razorpay error: ", error);
            that.onFailed(error);
            // alert(error.description + ' (Error ' + error.code + ')')
        };
        // RazorpayCheckout.open(options, this.successCallback(), cancelCallback);
        RazorpayCheckout.on('payment.success', successCallback);
        RazorpayCheckout.on('payment.cancel', cancelCallback);
        RazorpayCheckout.open(options);
    };
    PaytmwalletloginPage.prototype.showAlert = function (id) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: "Payment successfull.. payment Id - " + id,
            buttons: [{
                    text: 'Okay',
                    handler: function () {
                        _this.updateExpDate();
                    }
                }]
        });
        alert.present();
    };
    PaytmwalletloginPage.prototype.onFailed = function (err) {
        var _this = this;
        console.log(err);
        var alert = this.alertCtrl.create({
            message: 'Transaction Failed! Please try again with valid credentials',
            buttons: [{
                    text: 'Try Again',
                    handler: function () {
                        _this.goBack();
                    }
                }]
        });
        alert.present();
    };
    PaytmwalletloginPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PaytmwalletloginPage.prototype.updateExpDate = function () {
        var _this = this;
        debugger;
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        var expDate = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("updated expiry date: ", expDate);
        var url = this.apiCall.mainUrl + 'devices/deviceupdate';
        var payload = {
            // _id: this.parameters.Device_ID,
            // _id: this.parameters._id,
            // expiration_date: new Date(expDate).toISOString()
            "_id": this.parameters._id,
            "devicename": this.parameters.Device_Name,
            "drname": this.parameters.driver_name,
            "deviceid": this.parameters.Device_ID,
            "sim": this.parameters.sim_number,
            "iconType": null,
            "dphone": this.parameters.contact_number,
            "speed": this.parameters.SpeedLimit,
            "vehicleGroup": this.parameters.groupstaus_id,
            "device_model": this.parameters.device_model._id,
            "expdate": new Date(expDate).toISOString(),
            "renew_by": this.parameters.supAdmin,
            "user": this.parameters.user._id,
            "vehicleType": this.parameters.vehicleType._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdatatwo(url, payload)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log("respData of expiry date updation: ", respData);
            var toast = _this.toastCtrl.create({
                message: 'The expired device is renewed successfully!!',
                duration: 1000,
                position: 'middle'
            });
            toast.onDidDismiss(function () {
                _this.navCtrl.pop();
            });
            toast.present();
        }, function (err) {
            console.log("oops got error: ", err);
            _this.apiCall.stopLoading();
        });
    };
    PaytmwalletloginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-paytmwalletlogin',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/auto-telematics/src/pages/add-devices/paytmwalletlogin/paytmwalletlogin.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Renewal Method" | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content no-padding>\n  <ion-grid text-center *ngIf="this.islogin.isSuperAdmin == false">\n    <ion-row>\n      <ion-col>\n        ** For renewal please make the payment on 7507500582 via Gpay/ PhonePe. **\n          <!-- <p>Renewal Amount &nbsp;&nbsp; 2400</p> -->\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid text-center *ngIf="this.islogin.isSuperAdmin == true">\n    <ion-row>\n      <ion-col>\n        Yor are in admin mode.\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- <ion-card class="welcome-card">\n    <img src="assets/imgs/dc-Cover-u0b349upqugfio195s4lpk8144-20190213120303.Medi.jpeg">\n    <ion-card-header>\n\n      <ion-card-title>Vehicle Name - {{parameters.Device_Name}}</ion-card-title>\n      <ion-row>\n        <ion-col>\n          Total Payment\n        </ion-col>\n        <ion-col>\n          {{currencyIcon}}{{paymentAmount/100}}\n        </ion-col>\n      </ion-row>\n    </ion-card-header>\n    <ion-card-content>\n\n      <button ion-button full color="gpsc" (click)="payWithRazor()">Pay with RazorPay</button>\n    </ion-card-content> -->\n  <!-- </ion-card> -->\n  <!-- <ion-list lines="none">\n    <ion-list-header>\n      <ion-label>Resources</ion-label>\n    </ion-list-header>\n    <ion-item href="https://github.com/razorpay/razorpay-cordova">\n      <ion-icon slot="start" color="medium" name="book"></ion-icon>\n      <ion-label>Razorpay Plugin Documentation</ion-label>\n    </ion-item>\n    <ion-item href="https://medium.com/enappd/how-to-integrate-razorpay-in-ionic-4-apps-and-pwa-612bb11482d9">\n      <ion-icon slot="start" color="medium" name="grid"></ion-icon>\n      <ion-label>Ionic 4 - Razorpay Integration</ion-label>\n    </ion-item>\n    <ion-item href="https://store.enappd.com">\n      <ion-icon slot="start" color="medium" name="color-fill"></ion-icon>\n      <ion-label>More Ionic 4 Starters</ion-label>\n    </ion-item>\n  </ion-list> -->\n\n  <!-- <div style="padding: 0% 10% 0% 10%;">\n\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">{{ "Paytm" | translate }}</h4>\n    <ion-item style="padding-left: 0px;">\n      <ion-input [disabled]=\'inputform\' [(ngModel)]="paytmnumber" type="number" placeholder="Mobile Number*">\n      </ion-input>\n    </ion-item>\n    <p style="color:grey">{{ "Linking wallet is one time process. Next time, checkout will be a breeze!" | translate }}\n    </p>\n  </div>\n  <button [disabled]=\'inputform\' ion-button full color="gpsc"\n    style="height: 35px;width: 45%;margin-left: 30%;font-size: 15px;color: white;margin-top: 15px;color: rgb(255, 255, 255);"\n    (click)="paytmwallet()">{{ "Continue" | translate }}</button>\n  <ion-row style="padding: 2% 10% 0% 10%;" *ngIf="successresponse">\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">{{ "Enter verification code" | translate }}</h4>\n    <p style="margin-top: 0px;color: grey;margin-bottom: 0;">{{ "Paytm has send verification code on" | translate }}\n      {{paytmnumber}} {{ "via sms Please enter it below, and you are done!" | translate }}</p>\n    <ion-item style="padding: 0px;">\n      <ion-input [(ngModel)]="paytmotp" type="number" placeholder="Enter OTP"></ion-input>\n    </ion-item>\n    <ion-col col-sm-6>\n      <button ion-button full\n        style="height: 35px;background: rgb(220, 220, 220);font-size: 15px;transition: none 0s ease 0s;margin-top: 15px;color: #4a4747;"\n        (click)="resndOTP()">{{ "Resend" | translate }}</button>\n    </ion-col>\n    <ion-col col-sm-6>\n      <button ion-button full [disabled]="paytmotp == undefined"\n        style="height: 35px;background: rgb(220, 220, 220);font-size: 15px;transition: none 0s ease 0s;margin-top: 15px;color: #4a4747;"\n        (click)="paytmAuthantication()">{{ "Proceed" | translate }}</button>\n    </ion-col>\n  </ion-row> -->\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/auto-telematics/src/pages/add-devices/paytmwalletlogin/paytmwalletlogin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], PaytmwalletloginPage);
    return PaytmwalletloginPage;
}());

//# sourceMappingURL=paytmwalletlogin.js.map

/***/ })

});
//# sourceMappingURL=68.js.map