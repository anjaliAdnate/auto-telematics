import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import { CallNumber } from '@ionic-native/call-number';
import * as moment from 'moment';
import { IonPullUpFooterState } from 'ionic-pullup';

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage implements OnInit {

  contact_data: any = {};
  contactusForm: FormGroup;
  submitAttempt: boolean;
  islogin: any;
  tickets: any[] = [];
  datetimeStart: any;
  datetimeEnd: any;
  showDatePanel: boolean = false;
  footerState: IonPullUpFooterState;
  openNum: number = 0;
  closeNum: number = 0;
  inprogressNum: number = 0;
  tempTickets: any[] = [];
  selectedVehicle: any;
  device_id: any = [];
  device_name: any =[];
  portstemp: any;
  devices: any;
  number: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public api: ApiServiceProvider,
    public toastCtrl: ToastController,
    public plt: Platform
    // private callNumber: CallNumber
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    //this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeStart = moment().startOf('month').format();
    this.datetimeEnd = moment({ hours: 0 }).add(1, 'days').format();
    //this.datetimeEnd = moment().format();
    //new Date(a).toISOString();
    this.contactusForm = formBuilder.group({
      name: ['', Validators.required],
      mail: ['', Validators.email],
      mobno: ['', Validators.required],
      note: ['', Validators.required]
    });
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter ContactUsPage');
  }

  ngOnInit() {
    this.viewTicket();
    this.getdevices();
  }

  getdevices() {

    var baseURLp = this.api.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    //this.api.startLoading().present();
    this.api.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        //this.api.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.api.stopLoading();
          console.log(err);
        });
  }

  call(num) {
    // this.callNumber.callNumber(num, true)
    //   .then(res => console.log('Launched dialer!', res))
    //   .catch(err => console.log('Error launching dialer', err));
  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }
  contactUs() {
    this.submitAttempt = true;
    if (this.contactusForm.valid) {

      // this.contact_data = {
      //   "user": this.islogin._id,
      //   // "email": this.contactusForm.value.mail,
      //   "msg": this.contactusForm.value.note,
      //   // "phone": (this.contactusForm.value.mobno).toString(),

      //   // "dealerid": this.islogin.email,
      //   // "dealerName": this.islogin.fn + ' ' + this.islogin.ln
      //   // "dealerid": "gaurav.gupta@adnatesolutions.com",
      //   // "dealerName": "Gaurav Gupta"
      // }
      // var payData = {
      //   "user": this.islogin._id,
      //   // "email": this.contactusForm.value.mail,
      //   "msg": this.contactusForm.value.note,
      // }
      var payData = {
        "user": this.islogin._id,
        // "email": this.contactusForm.value.mail,
        "msg": this.contactusForm.value.note,
        "fileName":"autotel.json",
        //"fileName":"sanskarvts.json",
        "DeviceIMEI": this.device_id,
        "VehicleNo": this.device_name,
        "mobile": this.contactusForm.value.mobno,
        "email": this.contactusForm.value.mail
      }
      this.api.startLoading().present();
      // this.api.contactusApi(this.contact_data)
      this.api.createTicketApi(payData)
        .subscribe(data => {
          this.api.stopLoading();
          console.log(data.message)
          if (data.message == 'saved response') {
            let toast = this.toastCtrl.create({
              // message: 'Your request has been submitted successfully. We will get back to you soon.',
              message: 'Your ticket has been submitted successfully. We will get back to you soon.',
              position: 'bottom',
              duration: 3000
            });

            // toast.onDidDismiss(() => {
            //   console.log('Dismissed toast');
            //   // this.contactusForm.reset();
            //   this.navCtrl.setRoot(ContactUsPage);
            // });

            toast.present();
            this.navCtrl.setRoot(ContactUsPage);
          } else {
            let toast = this.toastCtrl.create({
              message: 'Something went wrong. Please try after some time.',
              position: 'bottom',
              duration: 3000
            });

            toast.onDidDismiss(() => {
              console.log('Dismissed toast');
              this.navCtrl.setRoot(ContactUsPage);
            });

            toast.present();
          }

        },
          error => {
            this.api.stopLoading();
            console.log(error);
          });
    }
  }

  getSummaarydevice(selectedVehicle) {
    this.device_id = [];
    this.device_name = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.device_id.push(selectedVehicle[t].Device_ID)
          this.device_name.push(selectedVehicle[t].Device_Name)
        }
      } else {
        this.device_id.push(selectedVehicle[0].Device_ID)
        this.device_name.push(selectedVehicle[0].Device_Name)
      }
    } else return;
    console.log("selectedVehicle=> ", this.device_id)
  }

  addTicket() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  viewTicket() {
    var _bUrl = this.api.mainUrl + "customer_support/getCustomerQuery";
    var payload = {
      "draw": 1,
      "columns": [
        {
          "data": "_id"
        },
        {
          "data": "superAdmin"
        },
        {
          "data": "dealer"
        },
        {
          "data": "ticketId"
        },
        {
          "data": "message"
        },
        {
          "data": "assigned_to"
        },
        {
          "data": "assigned_to.first_name"
        },
        {
          "data": "assigned_to.last_name"
        },
        {
          "data": "support_status"
        },
        {
          "data": "posted_on"
        },
        {
          "data": "posted_by"
        },
        {
          "data": "posted_by.first_name"
        },
        {
          "data": "posted_by.last_name"
        },
        {
          "data": "posted_by.phone"
        },
        {
          "data": "posted_by.email"
        },
        {
          "data": "role"
        },
        {
          "data": "user"
        }
      ],
      "order": [
        {
          "column": 3,
          "dir": "desc",

        }
      ],
      "start": 0,
      "length": 50,
      "search": {
        "value": "",
        "regex": false
      },
      "op": {},
      "select": [],
      "find": {
        "posted_by": this.islogin._id,
        "posted_on": {
          "$gte": {
            "_eval": "date",
            "value": new Date(this.datetimeStart).toISOString()
          },
          "$lte": {
            "_eval": "date",
            "value": new Date(this.datetimeEnd).toISOString()
          }
        }
      }
    }
    this.tickets = [];
    this.openNum = 0;
    this.closeNum = 0;
    this.inprogressNum = 0;
    this.api.startLoading().present();
    this.api.urlpasseswithdata(_bUrl, payload)
      .subscribe(data => {
        this.api.stopLoading();
        console.log("tickets: ", data)
        this.tickets = data.data;
        this.tempTickets = data.data;
        for (var i = 0; i < data.data.length; i++) {
          if (data.data[i].support_status == 'OPEN') {
            this.openNum += 1;
          } else if (data.data[i].support_status == 'CLOSE') {
            this.closeNum += 1;
          } else if (data.data[i].support_status == 'IN PROGRESS') {
            this.inprogressNum += 1;
          }
        }
      },
        err => {
          this.api.stopLoading();
          console.log("getting err while getting data: ", err)
        })
  }

  loadContent(key) {
    if (key === 'PROGRESS') {
      this.tickets = [];
      for (var i = 0; i < this.tempTickets.length; i++) {
        if (this.tempTickets[i].support_status === 'IN PROGRESS') {
          this.tickets.push(this.tempTickets[i]);
        }
      }
    } else if (key === 'OPEN') {
      this.tickets = [];
      for (var r = 0; r < this.tempTickets.length; r++) {
        if (this.tempTickets[r].support_status === 'OPEN') {
          this.tickets.push(this.tempTickets[r]);
        }
      }
    } else if (key === 'CLOSE') {
      this.tickets = [];
      for (var v = 0; v < this.tempTickets.length; v++) {
        if (this.tempTickets[v].support_status === 'CLOSE') {
          this.tickets.push(this.tempTickets[v]);
        }
      }
    }
  }

  onClickChat() {

    this.navCtrl.push('ChatPage', {
      params: this.islogin,
      isCustomer: !this.islogin.isSuperAdmin
    })
  }

  calll() {
    console.log("indside");
    this.number = JSON.stringify(this.islogin.Dealer_ID.phone)
    console.log("ckh number", this.number);
    if (this.number !== "" || this.number !== undefined) {
      if (this.plt.is('android')) {
        console.log("is android")
        window.open('tel:' + this.number, '_system');
      }
      // else if (this.plt.is('ios')) {
      //   console.log("is ios")
      //   // alert(number)
      //   this.callNumber(number.toString(), true)
      //     .then(res => console.log('Launched dialer!', res))
      //     .catch(err => console.log('Error launching dialer', err));
      // }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }

}
