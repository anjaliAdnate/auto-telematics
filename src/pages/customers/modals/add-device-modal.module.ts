import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDeviceModalPage } from './add-device-modal';
import { TranslateModule } from '@ngx-translate/core';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    AddDeviceModalPage
  ],
  imports: [
    IonicPageModule.forChild(AddDeviceModalPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ]
})
export class AddDeviceModalPageModule {}
