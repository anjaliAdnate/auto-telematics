import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
// import { SMS } from '@ionic-native/sms';
import * as moment from 'moment';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController, PopoverController, ViewController, Navbar, Platform, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { TranslateService } from '@ngx-translate/core';
import { ImmobilizeModelPage } from './immobilize-modal';
import { DrawerState } from 'ion-bottom-drawer';
// import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { TimePickerModal } from '../profile/settings/notif-setting/time-picker/time-picker';
import { CallNumber } from '@ionic-native/call-number';
//import { PARAMETERS } from '@angular/core/src/util/decorators';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as TinyURL from 'tinyurl';

@IonicPage()
@Component({
  selector: 'page-add-devices',
  templateUrl: 'add-devices.html',
})
export class AddDevicesPage implements OnInit {
  @ViewChildren("step") steps: QueryList<ElementRef>;
  public loadProgress: number = 0;
  @ViewChildren('myGauge') myGauge: QueryList<ElementRef>;
  islogin: any;
  isDealer: any;
  islogindealer: string;
  stausdevice: string;
  device_types: { vehicle: string; name: string; }[];
  GroupType: { vehicle: string; name: string; }[];
  GroupStatus: { name: string; }[];
  allDevices: any = [];
  allDevicesSearch: any = [];

  socket: any;
  veh: any;
  isdevice: string;
  userPermission: any;
  option_switch: boolean = false;
  dataEngine: any;
  DeviceConfigStatus: any;
  messages: string;
  editdata: any;
  responseMessage: string;
  searchCountryString = ''; // initialize your searchCountryString string empty
  countries: any;
  latLang: any;
  showActionSheet: boolean;
  drawerState1 = DrawerState.Docked;
  showFooter: boolean;
  dockedHeight1 = 300;
  distanceTop1 = 378;
  minimumHeight1 = 0;
  condition: string = 'gpsc';
  condition1: string = 'light';
  condition2: string = 'light';
  tttime: number;
  resToken: any;
  deviceDeatils: any = {};

  @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
  @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;
  // @ViewChild('foo', { read: ElementRef }) foo : ElementRef;
  // fooChart: any;
  @ViewChild(Navbar) navBar: Navbar;
  page: number = 0;
  limit: number = 8;
  ndata: any;
  searchItems: any;
  searchQuery: string = '';
  items: any[];
  deivceId: any;
  immobType: any;
  respMsg: any;
  commandStatus: any;
  intervalID: any;
  now: any;
  datePipe: any;
  isSuperAdmin: any;
  clicked: boolean = false;
  checkedPass: string;
  datetimeEnd: string;
  datetimeStart: string;
  liveDataShare: any;

  // superAdmin: boolean;
  dealer_Permission: boolean;
  checkIfStat: string = "ALL";
  public toggled: boolean = false;
  progressIntervalId: any;
  drawerData: any;
  intervalid123: any;
  // sqliteObj: SQLiteObject;
  devIcon: string;
  all_devices: any = [];
  singleVehicleCount: number;
  t_veicle_count: any;
  showEmptyStatus: boolean = false;
  distanceReport: any;
  mili: any;
  a: any;
  d: any;
  n:any;
  supid: any;
  relay_timer: any;

  public toggle(): void {
    this.toggled = !this.toggled;
  }
  cancelSearch() {
    // this.toggle();
    this.toggled = false;
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    // private sms: SMS,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private geocoderApi: GeocoderProvider,
    private plt: Platform,
    public translate: TranslateService,
    private events: Events,
    // private sqlite: SQLite,
    private callNumber: CallNumber,
    private socialSharing: SocialSharing,


  ) {
    //this.getDistanceReport();
    this.toggled = false;
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log("start", this.datetimeStart);
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log("end", this.datetimeEnd);
    // this.datetimeStart = moment({ hours: 0 }).format(); // yesterday date with 12:00 am
    // console.log("today time: ", this.datetimeStart)
    // this.datetimeEnd = moment({ hours: 0 }).format(); // today date and time with 12:00am
    if (localStorage.getItem('Total_Vech') !== null) {
      this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
    }
    // this.superAdmin = this.islogin.isSuperAdmin;
    // this.adbtn = localStorage.getItem("dlrchk");
    // this.dealer_Permission = this.islogin.device_add_permission;

    this.isDealer = this.islogin.isDealer;
    this.isSuperAdmin = this.islogin.isSuperAdmin;
    this.islogindealer = localStorage.getItem('isDealervalue');
    if (this.isDealer == false && this.isSuperAdmin == false) {
      this.dealer_Permission = false;
    } else {
      this.dealer_Permission = this.islogin.device_add_permission;
      console.log("dealer_Permission devices => " + this.dealer_Permission);
    }
    if (navParams.get("label") && navParams.get("value")) {
      this.stausdevice = localStorage.getItem('status');
      this.checkIfStat = this.stausdevice;
      this.singleVehicleCount = navParams.get("value");
    } else {
      this.stausdevice = undefined;
    }

    ////////////
    this.events.subscribe("Released:Dismiss", () => {
      this.getdevices();
      // this.getdevicesTemp();
    });
    //this.getDistanceReport();
  }
  showDrawer: boolean = false;
  shouldBounce = true;
  dockedHeight = 220;
  distanceTop = 556;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 0;

  showBottomDrawer(data) {
    console.log('drawerData', data)
    this.drawerData = data;
    // if(this.showDrawer) {
    //   this.showDrawer = false;
    // }
    //  setTimeout(() => {
    this.showDrawer = true;
    this.drawerState = DrawerState.Docked;
    //  }, 500);
  }

  fonctionTest(d) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    const modal = this.modalCtrl.create(TimePickerModal, {
      data: d,
      key: 'parking'
    });
    modal.onDidDismiss((data) => {
      // console.log(data);
      // this.getdevicesTemp123();
      this.getdevices();
    })
    modal.present();
  }

  towAlertCall(d) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    const modal = this.modalCtrl.create(TimePickerModal, {
      data: d,
      key: 'tow'
    });

    modal.onDidDismiss((data) => {
      // console.log(data);
      // this.getdevicesTemp123();
      this.getdevices();
    })
    modal.present();
  }

  ngOnInit() {
    this.now = new Date().toISOString();
    // this.getdevicesTemp();
    // this.getDataFromSQLiteDB();

    // this.intervalid123 = setInterval(() => {
    //   this.page = 0;
    //   this.getdevicesTemp123();
    // }, 30000);
    // localStorage.setItem('getDevicesInterval_ID', JSON.stringify(this.intervalid123))
  }

  ngOnDestroy() { }
  hideMe: boolean = false;
  // ionViewDidLoad() {
  //   this.getdevices();
  // }
  ionViewDidEnter() {
    this.getDefaultUserSettings();
    this.getDefaultUserSetting();
    this.navBar.backButtonClick = (ev: UIEvent) => {
      console.log('this will work in Ionic 3 +');
      this.hideMe = true;
      this.navCtrl.pop({
        animate: true, animation: 'transition-ios', direction: 'back'
      });
    }
    this.getdevices();
    this.cancelSearch();
    // this.getdevicesTemp();

    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              this.navCtrl.setRoot('DashboardPage')
            }
          }
        }
      }
    }

  }
  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }

  getDefaultUserSetting() {
    this.apiCall.startLoading().present();
    // if (this.islogin.isSuperAdmin == true) {
    //     this.supid = this.islogin._id;
    // } else {
    //   this.supid = this.islogin.supAdmin
    // }
    this.apiCall.getRenewalChareges(this.islogin._id)
    .subscribe(data => {
      this.apiCall.stopLoading();
      console.log("chk resp ", data);
      if(data.cust.user_settings) {
        console.log("relay timer", data.cust.user_settings.relay_timer);
        this.relay_timer = data.cust.user_settings.relay_timer
        console.log("finalPrint", this.relay_timer);
      } else {
        console.log("there is no user setting object in api");
      }

    })
  }

  // getDataFromSQLiteDB() {
  //   let that = this;
  //   this.sqlite.create({
  //     name: 'oneqlik_vts.db',
  //     location: 'default'
  //   }).then((db: SQLiteObject) => {
  //     db.executeSql(`
  //     CREATE TABLE IF NOT EXISTS vehicle_list(
  //       _id VARCHAR,
  //       Device_Name TEXT,
  //       Device_ID BIGINT,
  //       supAdmin VARCHAR,
  //       Dealer VARCHAR,
  //       expiration_date VARCHAR,
  //       status_updated_at VARCHAR,
  //       fuel_percent INT,
  //       currentFuel INT,
  //       last_speed INT,
  //       created_on VARCHAR,
  //       today_odo FLOAT,
  //       contact_number BIGINT,
  //       iconType TEXT,
  //       vehicleType TEXT,
  //       status TEXT,
  //       last_lat FLOAT,
  //       last_lng FLOAT)`, [])
  //       .then(res => {
  //         db.executeSql('SELECT * FROM vehicle_list', [])
  //           .then(res => {
  //             that.loadProgress = 100;
  //             let expiredDevices = [];
  //             let nodataDevices = [];
  //             that.runningData = []; that.idlingData = []; that.stoppedData = []; that.outOfReachData = []; that.all_devices = [];

  //             if (res.rows.length > 0) {
  //               console.log("total vehicle count: ", that.t_veicle_count)
  //               // if(res.rows.length === that.t_veicle_count) {

  //               // }
  //               for (var i = 0; i < res.rows.length; i++) {
  //                 console.log('icontype: ', res.rows.item(i).iconType);
  //                 if (res.rows.item(i).status === 'RUNNING') {
  //                   that.runningData.push(res.rows.item(i));
  //                 } else if (res.rows.item(i).status === 'IDLING') {
  //                   that.idlingData.push(res.rows.item(i));
  //                 } else if (res.rows.item(i).status === 'STOPPED') {
  //                   that.stoppedData.push(res.rows.item(i));
  //                 } else if (res.rows.item(i).status === 'OUT OF REACH') {
  //                   that.outOfReachData.push(res.rows.item(i));
  //                 } else if (res.rows.item(i).status === 'Expired') {
  //                   expiredDevices.push(res.rows.item(i));
  //                 } else if (res.rows.item(i).status === 'NO DATA') {
  //                   nodataDevices.push(res.rows.item(i));
  //                 }
  //                 that.all_devices.push(res.rows.item(i))

  //               }
  //               if (this.checkIfStat === 'ALL') {
  //                 this.allDevicesSearch = that.all_devices;
  //               } else if (this.checkIfStat === 'RUNNING') {
  //                 if (this.runningData.length !== this.singleVehicleCount) {
  //                   this.getdevicesTemp123();
  //                 } else {
  //                   this.allDevicesSearch = this.runningData;
  //                 }
  //               } else if (this.checkIfStat === 'IDLING') {
  //                 // this.allDevicesSearch = this.idlingData;
  //                 if (this.idlingData.length !== this.singleVehicleCount) {
  //                   this.getdevicesTemp123();
  //                 } else {
  //                   this.allDevicesSearch = this.idlingData;
  //                 }
  //               } else if (this.checkIfStat === 'OUT OF REACH') {
  //                 // this.allDevicesSearch = this.outOfReachData;
  //                 if (this.outOfReachData.length !== this.singleVehicleCount) {
  //                   this.getdevicesTemp123();
  //                 } else {
  //                   this.allDevicesSearch = this.outOfReachData;
  //                 }
  //               } else if (this.checkIfStat === 'STOPPED') {
  //                 // this.allDevicesSearch = this.stoppedData;
  //                 if (this.stoppedData.length !== this.singleVehicleCount) {
  //                   this.getdevicesTemp123();
  //                 } else {
  //                   this.allDevicesSearch = this.stoppedData;
  //                 }
  //               } else if (this.checkIfStat === 'Expired') {
  //                 // this.allDevicesSearch = expiredDevices;
  //                 if (expiredDevices.length !== this.singleVehicleCount) {
  //                   this.getdevicesTemp123();
  //                 } else {
  //                   this.allDevicesSearch = expiredDevices;
  //                 }
  //               } else if (this.checkIfStat === 'NO DATA') {
  //                 // this.allDevicesSearch = nodataDevices;
  //                 if (nodataDevices.length !== this.singleVehicleCount) {
  //                   this.getdevicesTemp123();
  //                 } else {
  //                   this.allDevicesSearch = nodataDevices;
  //                 }
  //               }
  //             } else {
  //               this.getdevicesTemp123();
  //             }
  //           })
  //           .catch(e => console.log(e));
  //       })
  //       .catch(e => {
  //         console.log(e)
  //       });
  //   })
  // }

  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    this.checkIfStat = ev._value;
    if (this.checkIfStat !== 'ALL') {
      this.stausdevice = ev._value
    } else {
      this.stausdevice = undefined;
    }

    this.page = 0;
    this.loadProgress = 100;
    this.getdevices();
  }

  getdevices() {
    this.showEmptyStatus = false;
    this.showDrawer = false;
    var baseURLp;

    let mainUrlNew
    if (localStorage.getItem('BASE_URL') != null) {
      mainUrlNew = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    }

    if (this.stausdevice) {
      baseURLp = (mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?email=' + this.islogin.email + '&id=' + this.islogin._id + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
    }
    else {
      baseURLp = (mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.progressBar();
    if (this.loadProgress == 100) {
      this.loadProgress = 0;
    }
    // this.apiCall.toastMsgStarted();
    // this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        // this.apiCall.toastMsgDismised();
        let that = this;
        that.loadProgress = 100;
        clearInterval(that.progressIntervalId);
        this.allDevicesSearch = [];
        this.ndata = data.devices;
        this.allDevices = this.ndata;
        this.allDevicesSearch = this.ndata;
        // this.allDevicesSearch = this.ndata;
        if (this.allDevicesSearch.length === 0) {
          this.showEmptyStatus = true;
        }
        that.userPermission = JSON.parse(localStorage.getItem('details'));
        if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
          that.option_switch = true;
        } else {
          if (localStorage.getItem('isDealervalue') == 'true') {
            that.option_switch = true;
          } else {
            if (that.userPermission.isDealer == false) {
              that.option_switch = false;
            }
          }
        }
      },
        err => {
          console.log("error=> ", err);
          let that = this;
          this.showEmptyStatus = false;
          clearInterval(that.progressIntervalId);
        });
  }

  AllData: any = [];
  runningData: any = [];
  stoppedData: any = [];
  idlingData: any = [];
  outOfReachData: any = [];

  getdevicesTemp123() {
    var baseURLp;
    if (this.checkIfStat != undefined && this.checkIfStat !== 'ALL') {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.checkIfStat + '&skip=' + this.page + '&limit=' + this.limit;
    } else {
      this.limit = 10;
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.progressBar();
    if (this.loadProgress == 100) {
      this.loadProgress = 0;
    }
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.loadProgress = 100;
        this.ndata = data.devices;
        this.allDevices = data.devices;
        // this.allDevicesSearch = this.ndata;
        let that = this;
        ////////////////////////////////////////////
        this.AllData = [];
        this.runningData = [];
        this.stoppedData = [];
        this.idlingData = [];
        this.outOfReachData = [];
        let expiredDevices = [];
        let nodataDevices = [];
        that.all_devices = [];
        for (var i = 0; i < data.devices.length; i++) {
          // console.log('icontype: ', res.rows.item(i).iconType);
          if (data.devices[i].status === 'RUNNING') {
            that.runningData.push(data.devices[i]);
          } else if (data.devices[i].status === 'IDLING') {
            that.idlingData.push(data.devices[i]);
          } else if (data.devices[i].status === 'STOPPED') {
            that.stoppedData.push(data.devices[i]);
          } else if (data.devices[i].status === 'OUT OF REACH') {
            that.outOfReachData.push(data.devices[i]);
          } else if (data.devices[i].status === 'Expired') {
            expiredDevices.push(data.devices[i]);
          } else if (data.devices[i].status === 'NO DATA') {
            nodataDevices.push(data.devices[i]);
          }
          that.all_devices.push(data.devices[i])

        }
        if (this.checkIfStat === 'ALL') {
          this.allDevicesSearch = that.all_devices;
        } else if (this.checkIfStat === 'RUNNING') {
          this.allDevicesSearch = this.runningData;
        } else if (this.checkIfStat === 'IDLING') {
          this.allDevicesSearch = this.idlingData;
        } else if (this.checkIfStat === 'OUT OF REACH') {
          this.allDevicesSearch = this.outOfReachData;
        } else if (this.checkIfStat === 'STOPPED') {
          this.allDevicesSearch = this.stoppedData;
        } else if (this.checkIfStat === 'Expired') {
          this.allDevicesSearch = expiredDevices;
        } else if (this.checkIfStat === 'NO DATA') {
          this.allDevicesSearch = nodataDevices;
        }
        console.log('running data: ', that.runningData.length);
        console.log('idling data: ', that.idlingData.length);
        console.log('out of reach data: ', that.outOfReachData.length);
        console.log('stopped data: ', that.stoppedData.length);
        ////////////////////////////////////////////
        that.userPermission = JSON.parse(localStorage.getItem('details'));
        if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
          that.option_switch = true;
        } else {
          if (localStorage.getItem('isDealervalue') == 'true') {
            that.option_switch = true;
          } else {
            if (that.userPermission.isDealer == false) {
              that.option_switch = false;
            }
          }
        }
      },
        err => {
          console.log("error=> ", err);
        });
  }

  settings() {
    let that = this;
    let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
      param: that.drawerData
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      that.callObjFunc(that.drawerData);

    })
  }

  geofence() {
    this.navCtrl.push('GeofencePage');
  }




  callObjFunc(d) {
    let that = this;
    var _bUrl = that.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log("updated device object=> " + resp);
        if (!resp) {
          return;
        } else {
          that.drawerData = resp;
        }
      })
  }

  // activateVehicle(data) {
  //   this.navCtrl.push("PaytmwalletloginPage", {
  //     "param": data
  //   })
  // }

  timeoutAlert() {
    let alerttemp = this.alertCtrl.create({
      message: "the server is taking much time to respond. Please try in some time.",
      buttons: [{
        text: this.translate.instant('Okay'),
        handler: () => {
          this.navCtrl.setRoot("DashboardPage");
        }
      }]
    });
    alerttemp.present();
  }

  showVehicleDetails(vdata) {

    this.navCtrl.push('VehicleDetailsPage', {
      param: vdata,
      option_switch: this.option_switch
    });
    this.showDrawer = false;
  }

  sharedevice(param) {
    let that = this;
    if (param == '15mins') {
      that.condition = 'gpsc';
      that.condition1 = 'light';
      that.condition2 = 'light';
      that.tttime = 15;
      // that.tttime  = (15 * 60000); //for miliseconds
    } else {
      if (param == '1hour') {
        that.condition1 = 'gpsc';
        that.condition = 'light';
        that.condition2 = 'light';
        that.tttime = 60;
        // that.tttime  = (1 * 3600000); //for miliseconds
      } else {
        if (param == '8hours') {
          that.condition2 = 'gpsc';
          that.condition = 'light';
          that.condition1 = 'light';
          that.tttime = (8 * 60);
          // that.tttime  = (8 * 3600000);
        }
      }
    }
  }


  shareLivetemp() {
    let that = this;
    if (that.tttime == undefined) {
      that.tttime = 15;
    }
    var data = {
      id: that.drawerData._id,
      imei: that.drawerData.Device_ID,
      sh: this.islogin._id,
      ttl: that.tttime   // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  liveShare() {
    let that = this;
    var link = 'https://www.oneqlik.in/' + "share/liveShare?t=" + that.resToken;
    TinyURL.shorten(link).then((res) => {
      // alert("tinyurl: " + res);
      that.socialSharing.share(that.islogin.fn + " " + that.islogin.ln + " has shared " + that.drawerData.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);

    })
    // that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    that.showActionSheet = true;
    that.showFooter = false;
    that.tttime = undefined;
  }

  doRefresh(refresher) {
    let that = this;
    that.page = 0;
    that.limit = 5;
    console.log('Begin async operation', refresher);
    this.getdevices();
    this.getdevicesTemp123();
    refresher.complete();
  }

  shareVehicle(d_data) {
    // let that = this;
    // const prompt = this.alertCtrl.create({
    //   title: 'Share Vehicle',
    //   inputs: [
    //     {
    //       name: 'device_name',
    //       value: d_data.Device_Name
    //     },
    //     {
    //       name: 'shareId',
    //       placeholder: this.translate.instant('Enter Email Id/Mobile Number')
    //     },
    //   ],
    //   buttons: [
    //     {
    //       text: this.translate.instant('Cancel'),
    //       handler: () => {
    //       }
    //     },
    //     {
    //       text: this.translate.instant('Share'),
    //       handler: (data) => {
    //         that.sharedevices(data, d_data)

    //       }
    //     }
    //   ]
    // });
    // prompt.present();
    let that = this;
    that.showActionSheet = false;
    // that.drawerHidden = false;
    that.drawerState1 = DrawerState.Docked;
    that.showFooter = true;
  }

  sharedevices(data, d_data) {
    let that = this;
    var devicedetails = {
      "did": d_data._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  showDeleteBtn(b) {
    // debugger;
    let that = this;
    if (localStorage.getItem('isDealervalue') == 'true') {
      return false;
    } else {
      if (b) {
        var u = b.split(",");
        for (let p = 0; p < u.length; p++) {
          if (that.islogin._id == u[p]) {
            return true;
          }
        }
      }
      else {
        return false;
      }
    }

  }

  sharedVehicleDelete(device) {
    let that = this;
    that.deivceId = device;
    let alert = that.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this share vehicle ?'),
      buttons: [{
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          that.removeDevice(that.deivceId._id);
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    });
    alert.present();
  }

  removeDevice(did) {
    this.apiCall.startLoading().present();
    this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
      .subscribe(data => {
        console.log(data)
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Shared Device was deleted successfully!'),
          duration: 1500
        });
        toast.onDidDismiss(() => {
          // this.getdevices();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  showSharedBtn(a, b) {
    // debugger
    if (b) {
      return !(b.split(",").indexOf(a) + 1);
    }
    else {
      return true;
    }
  }

  presentPopover(ev, data) {
    console.log("populated=> " + JSON.stringify(data))
    let popover = this.popoverCtrl.create(PopoverPage,
      {
        vehData: data
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => {
      this.getdevices();
    })

    popover.present({
      ev: ev
    });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      var baseURLp;
      if (that.stausdevice) {
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
      }
      else {
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
      }

      if (this.islogin.isSuperAdmin == true) {
        baseURLp += '&supAdmin=' + this.islogin._id;
      } else {
        if (this.islogin.isDealer == true) {
          baseURLp += '&dealer=' + this.islogin._id;
        }
      }
      that.ndata = [];
      that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(
          res => {
            if (res.devices.length > 0) {
              that.ndata = res.devices;
              for (let i = 0; i < that.ndata.length; i++) {
                // that.allDevices.push(that.ndata[i]);
                that.allDevicesSearch.push(that.ndata[i]);
              }
              // that.allDevicesSearch = that.allDevices;

            }
            infiniteScroll.complete();
          },
          error => {
            console.log(error);
          });

    }, 100);
  }

  progressBar() {
    // Test interval to show the progress bar
    this.progressIntervalId = setInterval(() => {
      if (this.loadProgress < 100) {
        this.loadProgress += 1;
        // console.log("progress: ", this.loadProgress);
      }
      else {
        clearInterval(this.loadProgress);
      }
    }, 100);
  }


  livetrack(device) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    localStorage.setItem("LiveDevice", "LiveDevice");
    let animationsOptions;
    if (this.plt.is('android')) {
      this.navCtrl.push('LiveSingleDevice', { device: device });
    } else {
      if (this.plt.is('ios')) {
        animationsOptions = {
          animation: 'ios-transition',
          duration: 1000
        }
        this.navCtrl.push('LiveSingleDevice', { device: device }, animationsOptions);
      }
    }
  }

  showHistoryDetail(device) {

    this.navCtrl.push('HistoryDevicePage', {
      device: device
    });
    if (this.showDrawer) {
      this.showDrawer = false;
    }
  }
  device_address(device, index) {
    let that = this;
    let tempcord = {};
    if (!device.last_location) {
      that.allDevicesSearch[index].address = "N/A";
      if (!device.last_lat && !device.last_lng) {
        that.allDevicesSearch[index].address = "N/A";
      } else {
        tempcord = {
          "lat": device.last_lat,
          "long": device.last_lng
        }
        this.apiCall.getAddress(tempcord)
          .subscribe(res => {
            if (res.message === "Address not found in databse") {
              this.geocoderApi.reverseGeocode(device.last_lat, device.last_lng)
                .then(res => {
                  var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                  that.saveAddressToServer(str, device.last_lat, device.last_lng);
                  that.allDevicesSearch[index].address = str;
                  // console.log("inside", that.address);
                })
            } else {
              that.allDevicesSearch[index].address = res.address;
            }
          })
      }
    } else {
      tempcord = {
        "lat": device.last_location.lat,
        "long": device.last_location.long
      }
      this.apiCall.getAddress(tempcord)
        .subscribe(res => {
          if (res.message === "Address not found in databse") {
            this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
              .then(res => {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
                // that.allDevices[index].address = str;
                that.allDevicesSearch[index].address = str;
                // console.log("inside", that.address);
              })
          } else {
            // that.allDevices[index].address = res.address;
            that.allDevicesSearch[index].address = res.address;
          }
        })
    }
  }

  // device_address(device, index) {
  //   let that = this;
  //   if (!device.last_location) {
  //     that.allDevices[index].address = "N/A";
  //     return;
  //   }
  //   let tempcord = {
  //     "lat": device.last_location.lat,
  //     "long": device.last_location.long
  //   }
  //   this.apiCall.getAddress(tempcord)
  //     .subscribe(res => {
  //       if (res.message === "Address not found in databse") {
  //         this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
  //           .then(res => {
  //             var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //             that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
  //             that.allDevices[index].address = str;
  //             // console.log("inside", that.address);
  //           })
  //       } else {
  //         that.allDevices[index].address = res.address;
  //       }
  //     })
  // }
  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  getDuration(device, index) {
    let that = this;
    that.allDevicesSearch[index].duration = "0hrs 0mins";
    if (!device.status_updated_at) {
      // console.log("did not found last ping on")
      that.allDevicesSearch[index].duration = "0hrs 0mins";
    } else if (device.status_updated_at) {
      // console.log("found last ping on")
      var a = moment(new Date().toISOString());//now
      var b = moment(new Date(device.status_updated_at).toISOString());
      var mins;
      mins = a.diff(b, 'minutes') % 60;
      that.allDevicesSearch[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
    }
  }

  // getDuration(device, index) {
  //   let that = this;
  //   that.allDevices[index].duration = "0hrs 0mins";
  //   if (!device.status_updated_at) {
  //     // console.log("did not found last ping on")
  //     that.allDevices[index].duration = "0hrs 0mins";
  //   } else if (device.status_updated_at) {
  //     // console.log("found last ping on")
  //     var a = moment(new Date().toISOString());//now
  //     var b = moment(new Date(device.status_updated_at).toISOString());
  //     var mins;
  //     mins = a.diff(b, 'minutes') % 60;
  //     that.allDevices[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
  //   }
  // }

  callSearch(ev) {
    var searchKey = ev.target.value;
    var _baseURL;

    if (this.islogin.isDealer == true) {
      _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
    } else {
      if (this.islogin.isSuperAdmin == true) {
        _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
      } else {
        _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
      }
    }

    this.apiCall.callSearchService(_baseURL)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        console.log("search result=> " + JSON.stringify(data))
        this.allDevicesSearch = data.devices;
        this.allDevices = data.devices;
        // console.log("fuel percentage: " + data.devices[0].fuel_percent)
      },
        err => {
          console.log(err);
          // this.apiCall.stopLoading();
        });
  }

  getDistanceReport(device, i) {

    //this.apiCall.startLoading().present();
    console.log("device id", device);
    var distanceReport = [];
    var user = device.user;
    if (typeof user === 'object') {
      var user_id = user._id;
    } else {
      var user_id = user;
    }
    this.apiCall.getDistanceReportApi(new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), user_id, device._id)
      .subscribe(data => {

        console.log("dataa", data)
        distanceReport = data;
        var dd123 = (distanceReport.length > 0) ? parseFloat(distanceReport[0].distance).toFixed(2) : 0;
        console.log("dataaaaaa", dd123);

        device['distance'] = dd123;
        // this.apiCall.stopLoading();
      }, error => {
        // this.apiCall.stopLoading();
        device['distance'] = 0;
        console.log(error);
      })
  }

  innerFunc(distanceReport) {
    let outerthis = this;
    var i = 0, howManyTimes = distanceReport.length;
    function f() {

      outerthis.allDevicesSearch.push({
        'distance': distanceReport[i].distance,
      });
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
    console.log("final data", this.allDevicesSearch);
  }

  checkImmobilizePassword() {
    const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(rurl, Var)
      .subscribe(data => {
        if (!data.engine_cut_psd) {
          this.checkedPass = 'PASSWORD_NOT_SET';
        } else {
          this.checkedPass = 'PASSWORD_SET';
        }
      })
  }

  checkPointsAvailability() {
    var url = this.apiCall.mainUrl + "users/checkPointAvailablity?u_id=" + this.islogin._id + "&demanded_points=0";
    this.apiCall.getSOSReportAPI(url)
      .subscribe((resp) => {
        console.log("points availability: ", resp);
      },
        err => {
          console.log("error ", err);
        })
  }

  IgnitionOnOff(d) {
    console.log("lastpingonn", d.last_ping_on);
    this.mili = new Date(d.last_ping_on).getTime()
    console.log("convert last update time to milisecond", this.mili);
    this.d = new Date();
    this.n = this.d.getTime();
    console.log("current time in mili second", this.n);
    this.a = this.n - this.mili;
    console.log("difference", this.a);
    if (this.a >= 900000) {
      var Popup = this.alertCtrl.create({
      title: this.translate.instant('Warning'),
      message: this.translate.instant('Device has not updated data since more than 15 mins'),
      buttons: [this.translate.instant('Okay')]
      });
      Popup.present();
  } else {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    let pModal = this.modalCtrl.create(ImmobilizeModelPage, {
      param: d
    });
    pModal.onDidDismiss(() => {
      this.getdevices();
    })
    pModal.present();
  }
    // debugger


  };

  // askForPassword(d) {
  //   const prompt = this.alertCtrl.create({
  //     title: 'Enter Password',
  //     message: "Enter password for engine cut",
  //     inputs: [
  //       {
  //         name: 'password',
  //         placeholder: 'Password'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         handler: data => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Proceed',
  //         handler: data => {
  //           console.log('Saved clicked');
  //           console.log("data: ", data)
  //           // if (data.password !== data.cpassword) {
  //           //   this.toastmsg("Entered password and confirm password did not match.")
  //           //   return;
  //           // }
  //           this.verifyPassword(data, d);
  //         }
  //       }
  //     ]
  //   });
  //   prompt.present();
  // }

  // verifyPassword(pass, d) {
  //   const ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
  //   var payLd = {
  //     "uid": this.islogin._id,
  //     "psd": pass.password
  //   }
  //   this.apiCall.startLoading().present();
  //   this.apiCall.urlpasseswithdata(ryurl, payLd)
  //     .subscribe(resp => {
  //       this.apiCall.stopLoading();
  //       console.log(resp);
  //       if (resp.message === 'password not matched') {
  //         this.toastmsg(resp.message)
  //         return;
  //       }
  //       this.serverLevelOnOff(d);
  //     },
  //       err => {
  //         this.apiCall.stopLoading();
  //       });
  // }

  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }

  // serverLevelOnOff(d) {
  //   let that = this;
  //   that.clicked = true;
  //   var data = {
  //     "imei": d.Device_ID,
  //     "_id": this.dataEngine._id,
  //     "engine_status": d.ignitionLock,
  //     "protocol_type": d.device_model.device_type
  //   }
  //   // this.apiCall.startLoading().present();
  //   this.apiCall.serverLevelonoff(data)
  //     .subscribe(resp => {
  //       // this.apiCall.stopLoading();
  //       console.log("ignition on off=> ", resp)
  //       this.respMsg = resp;

  //       // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
  //       this.intervalID = setInterval(() => {
  //         this.apiCall.callResponse(this.respMsg._id)
  //           .subscribe(data => {
  //             console.log("interval=> " + data)
  //             this.commandStatus = data.status;

  //             if (this.commandStatus == 'SUCCESS') {
  //               clearInterval(this.intervalID);
  //               that.clicked = false;
  //               // this.apiCall.stopLoadingnw();
  //               const toast1 = this.toastCtrl.create({
  //                 message: this.translate.instant('process has been completed successfully!'),
  //                 duration: 1500,
  //                 position: 'bottom'
  //               });
  //               toast1.present();
  //               // this.getdevices();
  //             }
  //           })
  //       }, 5000);
  //     },
  //       err => {
  //         this.apiCall.stopLoading();
  //         console.log("error in onoff=>", err);
  //         that.clicked = false;
  //       });
  // }

  dialNumber(number) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    console.log("check phone number: ", number)
    if (number !== "" || number !== undefined) {
      if (this.plt.is('android')) {
        console.log("is android")
        window.open('tel:' + number, '_system');
      } else if (this.plt.is('ios')) {
        console.log("is ios")
        // alert(number)
        this.callNumber.callNumber(number.toString(), true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }

  getItems(ev: any) {
    const val = ev.target.value.trim();
    this.allDevicesSearch = this.allDevices.filter((item) => {
      return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.allDevicesSearch);
  }

  onClear(ev) {
    this.getdevices();
    // this.getdevicesTemp();
    ev.target.value = '';
    // this.toggled = false;
  }

  openAdddeviceModal(cust) {
    let profileModal = this.modalCtrl.create('AddDeviceModalPage');
    //NavParams: this.allDevicesSearch;
    profileModal.onDidDismiss(data => {
      console.log(data);
      this.getdevices();
    });
    profileModal.present();
  }

  // IgnitionOnOff() {
  //   let pModal = this.modalCtrl.create('ImmobilizeModelPage');
  //   pModal.onDidDismiss(data => {
  //     console.log(data);
  //   })
  // }

  upload(vehData) {
    this.navCtrl.push('UploadDocPage', { vehData: vehData });
  }
  iconCheck(status, iconType) {

    var devStatus = status.split(" ");

    if ((iconType == 'car') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/car_blue_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/car_green_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/car_red_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/car_yellow_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/car_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'bike') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/bike_blue_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/bike_green_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/bike_red_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/bike_yellow_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/bike_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'bus') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/bus_blue.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/bus_green.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/bus_red.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/bus_yellow.jpg";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/bus_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'truck') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/truck_icon_blue.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/truck_icon_green.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/truck_icon_red.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/truck_icon_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/truck_icon_grey.png";
      return this.devIcon;
    }
    else if ((iconType == 'tractor') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/tractor_blue.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/tractor_green.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/tractor_red.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/tractor_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/tractor_gray.png";
      return this.devIcon;
    }
    else if ((!iconType) && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/car_blue_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/car_green_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/car_red_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/car_yellow_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/car_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'user') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    }
    else if ((iconType == 'jcb') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/jcb_blue.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/jcb_green.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/jcb_red.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/jcb_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/jcb_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'ambulance') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/ambulance_blue.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/ambulance_green.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/ambulance_red.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/ambulance_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/ambulance_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'auto') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/auto_blue.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/auto_green.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/auto_red.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/auto_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/auto_gray.png";
      return this.devIcon;
    }
    else {
      this.devIcon = "../../assets/imgs/noIcon.png";
      return this.devIcon
    }

  }
}

@Component({
  template: `
    <ion-list>
      <ion-item class="text-palatino" (click)="editItem()">
        <ion-icon name="create"></ion-icon>&nbsp;&nbsp;{{'edit' | translate}}
      </ion-item>
      <ion-item class="text-san-francisco" (click)="deleteItem()">
        <ion-icon name="trash"></ion-icon>&nbsp;&nbsp;{{'delete' | translate}}
      </ion-item>
      <ion-item class="text-seravek" (click)="shareItem()">
        <ion-icon name="share"></ion-icon>&nbsp;&nbsp;{{'share' | translate}}
      </ion-item>
    </ion-list>
  `
})


export class PopoverPage implements OnInit {
  contentEle: any;
  textEle: any;
  vehData: any;
  islogin: any;
  veh: any;
  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public translate: TranslateService
  ) {
    this.vehData = navParams.get("vehData");
    console.log("popover data=> ", this.vehData);

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + this.islogin);
  }

  ngOnInit() { }

  editItem() {
    console.log("edit")
    let modal = this.modalCtrl.create('UpdateDevicePage', {
      vehData: this.vehData
    });
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.viewCtrl.dismiss();
      // this.getdevices();
    })
    modal.present();
  }

  deleteItem() {
    let that = this;
    console.log("delete")
    let alert = this.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this vehicle ?'),
      buttons: [{
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          console.log(that.vehData.Device_ID)
          that.deleteDevice(that.vehData.Device_ID);
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    });
    alert.present();
  }


  deleteDevice(d_id) {
    this.apiCall.startLoading().present();
    this.apiCall.deleteDeviceCall(d_id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        var DeletedDevice = data;
        console.log(DeletedDevice);

        let toast = this.toastCtrl.create({
          message: this.translate.instant('Vehicle deleted successfully!'),
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.viewCtrl.dismiss();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  shareItem() {
    let that = this;
    console.log("share")
    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Share Vehicle'),
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'device_name',
          value: that.vehData.Device_Name
        },
        {
          name: 'shareId',
          placeholder: this.translate.instant('Enter Email Id/Mobile Number')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('Share'),
          handler: data => {
            console.log('Saved clicked');
            console.log("clicked=> ", data)

            that.sharedevices(data)

          }
        }
      ]
    });
    prompt.present();
  }

  // valScreen() {
  //   let that = this
  //   console.log("selected=>", that.vehData);
  //   localStorage.selectedVal = JSON.stringify(that.vehData);
  //   this.navCtrl.push('DailyReportNewPage');
  //   // if(this.vehData == "Device_Name" ) {
  //   //   console.log(this.vehData);
  //   //   this.navCtrl.push('DailyReportNewPage');
  //   // }
  // }


  sharedevices(data) {
    let that = this;
    console.log(data.shareId);
    var devicedetails = {
      "did": that.vehData._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        // var editdata = data;
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }
}
