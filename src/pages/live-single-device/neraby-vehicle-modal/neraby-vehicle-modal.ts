import { Component } from "@angular/core";
import { ViewController } from "ionic-angular";

@Component({
    selector: 'page-neraby-vehicle-modal',
    templateUrl: 'neraby-vehicle-modal.html'
})

export class NearbyVehicleModal {

    constructor(
        private viewCtrl: ViewController
    ) {

    }

    onClick(key) {
        this.dismiss(key);
    }

    dismiss(key) {
        this.viewCtrl.dismiss(key);
    }
}