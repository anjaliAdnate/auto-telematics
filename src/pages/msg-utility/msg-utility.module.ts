import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MsgUtilityPage } from './msg-utility';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    MsgUtilityPage,
  ],
  imports: [
    IonicPageModule.forChild(MsgUtilityPage),
    SelectSearchableModule
  ],
})
export class MsgUtilityPageModule {}
